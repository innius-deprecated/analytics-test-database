# analytics-test-database

package to facilitate integration tests against a local postgress database 

*note* there are (differences)[http://docs.aws.amazon.com/redshift/latest/dg/c_redshift-and-postgres-sql.html] between the local postgress database and the actual redshift db. 


## usage 
start a local postgres container 
```
	docker run -it --name postgres -e POSTGRES_PASSWORD=secret  -p 127.0.0.1:5439:5439 --rm postgres:latest  
```

and create a test query system
```
	qs := NewAnalyticsDatabase()
	
```
