package testdb

import (
	"fmt"
	"os"
	"testing"
	"time"

	trn "bitbucket.org/to-increase/go-trn"
	"github.com/stretchr/testify/assert"
)

func TestMain(t *testing.M) {
	os.Exit(t.Run())
}

func TestInitilized(t *testing.T) {
	qs, err := NewLocalAnalyticsDatabase()
	assert.NoError(t, err)
	assert.NotNil(t, qs)
	t.Run("create dataset", func(t *testing.T) {
		ts := time.Now()
		mtrn := trn.NewMachine("plantage.io").String()
		assert.NoError(t, qs.InsertSensorEvents(mtrn, "foo", map[time.Time]float32{ts: 10, ts: 20}))
		assert.NoError(t, qs.InsertSensorEvents(mtrn, "bar", map[time.Time]float32{ts: 10, ts: 20}))

		rows, err := qs.db.Query(fmt.Sprintf("select * from f_sensor_log where machine_id='%s'", mtrn))
		count := 0
		for rows.Next() {
			count++
		}
		assert.NoError(t, err)
		assert.Equal(t, 2, count)
	})
}
