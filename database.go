package testdb

import (
	"bytes"
	"text/template"
	"time"

	"github.com/jmoiron/sqlx"

	_ "github.com/lib/pq"
	"github.com/pkg/errors"

	"fmt"
)

type AnalyticsDatabase struct {
	db *sqlx.DB
}

const postgres = "postgres"

const schema = `
	CREATE TABLE IF NOT EXISTS f_sensor_log
	(
		dt_key					INTEGER,
		ti_key                  INTEGER,
		event_timestamp			TIMESTAMP without time zone NOT NULL,
		machine_id				VARCHAR(128) NOT NULL,
		sensor_id				VARCHAR(128) NOT NULL,
		sensor_instance_state	VARCHAR(30),
		sensor_reading_value	DECIMAL(10,4),
		sensor_reading_str		VARCHAR(30),
		sensor_complex_data		VARCHAR(250)
	);
	`

// NewLocalAnalyticsDatabase creates a local analytics database
func NewLocalAnalyticsDatabase() (*AnalyticsDatabase, error) {
	return NewAnalyticsDatabase("localhost", 5432, postgres, postgres, "secret")
}

// NewAnalyticsDatabase creates a new analytics database
func NewAnalyticsDatabase(host string, port int, dbname, user, password string) (*AnalyticsDatabase, error) {
	cs := fmt.Sprintf(
		"host=%v port=%v dbname=%v user=%v password=%v sslmode=disable", host, port, dbname, user, password)

	db, err := sqlx.Open(postgres, cs)
	if err != nil {
		return nil, errors.Wrap(err, "")
	}

	db.MustExec(schema)

	if err != nil {
		return nil, errors.Wrap(err, "")
	}
	return &AnalyticsDatabase{
		db: db,
	}, nil
}

// InsertSensorEvents inserts specified sensor events into the database
func (t *AnalyticsDatabase) InsertSensorEvents(m string, s string, values map[time.Time]float32) error {
	b, err := insertStatements(m, s, values)
	if err != nil {
		return err
	}

	res, err := t.db.Exec(b.String())
	if err != nil {
		return errors.Wrap(err, "could not execute the insert query")
	}
	cnt, err := res.RowsAffected()
	if err != nil {
		return errors.Wrap(err, "")
	}
	count := len(values)
	if cnt != int64(count) {
		return errors.Errorf("number of inserted records %d does not match the expected number %d", cnt, count)
	}
	return nil
}

// MakeTestDataSeries returns an expanded set with sensor values and time stamps
func MakeTestDataSeries(from, to int64, values []float32) map[time.Time]float32 {
	valueMap := map[time.Time]float32{}
	for i, v := range values {
		denom := float64(1)
		if len(values) > 1 {
			denom = float64(len(values) - 1)
		}
		time1 := int(float64(from) + ((float64(i) / denom) * float64(to-from)))
		timeStamp := time.Unix(int64(time1), 0)
		valueMap[timeStamp] = v
	}
	return valueMap
}

// InsertSensorDataSeries expands the provided series into sensor values and insert them into the database
func (t *AnalyticsDatabase) InsertSensorDataSeries(m string, s string, from int64, to int64, values []float32) error {
	return t.InsertSensorEvents(m, s, MakeTestDataSeries(from, to, values))
}

func insertStatements(m string, s string, values map[time.Time]float32) (bytes.Buffer, error) {
	const tmpl = `
		INSERT INTO f_sensor_log (event_timestamp, machine_id, sensor_id,sensor_reading_value) VALUES
		{{range .}}{{.}}
		{{end}};
	`
	count := len(values)
	lines := make([]string, count)
	i := 0
	for ts, v := range values {
		tsf := ts.UTC().Format("2006-01-02 15:04:05")
		lines[i] = fmt.Sprintf("('%s','%s','%s',%f)", tsf, m, s, v)
		if i < count-1 {
			lines[i] += ","
		}
		i++
	}
	tp := template.Must(template.New("foo").Parse(tmpl))
	var b bytes.Buffer
	err := tp.Execute(&b, lines)
	if err != nil {
		return b, errors.Wrap(err, "")
	}
	return b, nil
}
